# --- set these according to your configuration ---
LINUXSDL = /usr/include/SDL2
MINGWSDL = ../..
MACSDL = ~/Library/Frameworks/SDL2.framework/Versions/A/Headers
# -------------------------------------------------

TARGET = mtsedit
SRCS = $(filter-out bin2h.c,$(wildcard *.c))
OBJS = $(SRCS:.c=.o)

# MacOSX
ifneq ("$(wildcard $(MACSDL)/SDL.h)","")
CFLAGS = -I$(MACSDL)
FDIR=$(MACSDL:/Headers=)
FDIR=$(FDIR:/Versions/A=)
FDIR=$(FDIR:/SDL2.framework=)
LIBS = -F$(FDIR) -framework SDL2
PACKAGE = MacOSX
else
ifneq ("$(wildcard /Library/Frameworks/SDL*)","")
CFLAGS = -I/Library/Frameworks/SDL2.framework/Headers -I/Library/Frameworks/SDL2.framework/Versions/A/Headers
LIBS = -F/Library/Frameworks -framework SDL2
PACKAGE = MacOSX
else
# Linux
ifneq ("$(wildcard $(LINUXSDL)/SDL.h)","")
CFLAGS = -I$(LINUXSDL)
LIBS = -lSDL2
PACKAGE = Linux
else
# Windows MinGW
ifneq ("$(wildcard $(MINGWSDL)/i686-w64-mingw32/include/SDL2/SDL.h)","")
CFLAGS = -I$(MINGWSDL)/i686-w64-mingw32/include/SDL2
LIBDIRS = -static-libgcc -L$(MINGWSDL)/i686-w64-mingw32/lib
LIBS = -lSDL2 -luser32
OBJS += resource.o
PACKAGE = Win
CC = gcc
endif
endif
endif
endif

CFLAGS += -Wall -Wextra -ansi -pedantic -D_$(PACKAGE)_=1

all: configure data.h $(OBJS) $(TARGET)

configure:
ifeq ("$(LIBS)","")
	@echo "No headers found, ui driver cannot be detected. Install libsdl2-dev."
	@echo "Also check if the header files are in MINGSDL or MACSDL paths."
	@echo "Try to run 'find / -name SDL.h' in a terminal to find the header."
	@false
endif

data.h: bin2h.c icons.png font.psf.gz
	@$(CC) bin2h.c -o bin2h
	./bin2h icons.png font.psf.gz ../mt-mod/mtsedit >data.h
	@rm bin2h bin2h.exe 2>/dev/null || true

resource.o:
	windres -i ../etc/resource.rc -o resource.o

%: %.c main.h lang.h data.h
	$(CC) $(CFLAGS) $< -c $@

$(TARGET): $(OBJS)
	$(CC) $(LIBDIRS) $(OBJS) -o $(TARGET) $(LIBS)

install: $(TARGET)
	install -m 755 -g bin $(TARGET) /usr/bin
	@mkdir -p /usr/share/mtsedit
	cp ../data/* /usr/share/mtsedit
	cp ../etc/mtsedit.desktop /usr/share/applications
	cp ../etc/mtsedit.png /usr/share/icons/hicolor/32x32/apps

package:
ifeq ("$(PACKAGE)","Linux")
	@mkdir usr usr/bin usr/share usr/share/mtsedit usr/share/applications usr/share/icons usr/share/icons/hicolor usr/share/icons/hicolor/32x32 usr/share/icons/hicolor/32x32/apps
	@cp $(TARGET) usr/bin
	@cp -r ../data/* usr/share/mtsedit
	@cp ../etc/mtsedit.desktop usr/share/applications
	@cp ../etc/mtsedit.png usr/share/icons/hicolor/32x32/apps
	tar -czvf ../$(TARGET)-x86_64-linux.tgz usr
	@rm -rf usr
else
ifeq ("$(PACKAGE)","Win")
	@mkdir MTSEdit
	@cp $(TARGET).exe MTSEdit/$(TARGET).exe
	@cp ../etc/MTSEdit.lnk MTSEdit
	@cp $(MINGWSDL)/i686-w64-mingw32/bin/SDL2.dll MTSEdit/SDL2.dll
	@cp -r ../data MTSEdit/data
	zip -r ../$(TARGET)-i686-win.zip MTSEdit
	@rm -rf MTSEdit
else
	@mkdir MTSEdit.app MTSEdit.app/Contents MTSEdit.app/Contents/MacOS MTSEdit.app/Contents/Resources
	@cp $(TARGET) MTSEdit.app/Contents/MacOS
	@cp ../etc/Info.plist MTSEdit.app/Contents
	@cp ../etc/mtsedit.icns MTSEdit.app/Contents/Resources
	@cp -r ../data MTSEdit.app/Contents/Resources
	zip -r ../$(TARGET)-intel-macosx.zip MTSEdit.app
	@rm -rf MTSEdit.app
endif
endif

clean:
	@rm $(TARGET) $(TARGET).exe data.h *.o 2>/dev/null || true
