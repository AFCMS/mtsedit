/*
 * mtsedit/lang.h
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Multilanguage definitions
 *
 */

enum {
    INF_VERB = 0,
    INF_LANG,
    INF_DUMP,
    INF_PRE1,
    INF_PRE2,
    INF_BPRINT,
    INF_GEN,
    INF_BLK,
    INF_REMAP,
    INF_MAP,
    INF_MOD,
    INF_VOX,
    INF_OUT,
    LOADING,
    ERR_MEM,
    ERR_MOD,
    ERR_DATADIR,
    ERR_CSV,
    ERR_IMGCSV,
    ERR_IMG,
    ERR_NODE,
    ERR_SAVE,
    ERR_LOAD,
    ERR_BADFILE,
    ERR_PREVIEW,
    ERR_BPRINT,
    ERR_NOBIOM,
    SAVED,
    LOADED,
    PREVIEWSAVED,
    BPRINTSAVED,
    IMGSGEN,
    NORTH,
    CURRLAYER,
    LAYERPROB,
    GROUNDLEVEL,
    GND,
    LOAD,
    SAVE,
    SAVEPREVIEW,
    CHANGEORIENT,
    MIRROR,
    ADDY,
    DELY,
    ADDX,
    DELX,
    ADDZ,
    DELZ,
    UNDO,
    REDO,
    ZOOMIN,
    ZOOMOUT,
    BRUSH,
    ALLBLOCKS,
    LAYERUP,
    LAYERDOWN,
    ADDBLOCKS,
    SEARCH,
    LOADFROM,
    LOADBTN,
    SAVEAS,
    FILENAME,
    MAPPING,
    MAPGENTYPE,
    BIOMETYPE,
    SAVEBTN,
    TYPE,
    HOLLOW,
    RADIUS,
    WIDTH,
    LENGTH,
    HEIGHT,
    AIR,
    SELBLOCK,
    FILL,
    PAINT,
    SCROLL,
    GRID,
    HELP,

    /* must be the last */
    NUMTEXTS
};

#define NUMLANGS         9

