/*
 * mtsedit/save.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Save file window
 *
 */

#include "main.h"

int savelen, savepos, savefld = 0, savepal = 0, savemapgen = 0, savebiome = 0;
int biomepos = 0, biomeblk = -1, maxbiome = 0, biomescr = 0;
extern char cur[2];

/**
 * Redraw the Save file window
 */
void save_redraw()
{
    unsigned int i, j;
    int k, l;
    SDL_Rect rect;

    rect.x = 36; rect.y = 0; rect.w = screen->w - 36; rect.h = screen->h - font->height;
    SDL_FillRect(screen, &rect, theme[THEME_BG]);

    strmaxw = screen->w - 5;
    sdlprint((screen->w - 47 - mbstrlen(lang[SAVEAS]) * (font->width+1)) / 2 + 47, 4, THEME_INPUT, THEME_BG, lang[SAVEAS]);
    rect.y = 12 + font->height;
    sdlprint(42, rect.y, THEME_FG, THEME_BG, lang[FILENAME]);
    rect.x = 200; rect.w = screen->w - 205; rect.h = font->height + 2;
    SDL_FillRect(screen, &rect, theme[THEME_INPBG]);
    strmaxw = screen->w - 5;
    sdlprint(201,rect.y + 1, !savefld ? THEME_INPUT : THEME_INACT, THEME_INPBG, mtsfile);
    if(!savefld) {
        cur[0] = mtsfile[savepos] ? mtsfile[savepos] : ' ';
        sdlprint(201 + savepos * (font->width+1), rect.y + 1, THEME_INPBG, THEME_INPUT, cur);
    }
    rect.y = 16 + 2*font->height;
    sdlprint(42, rect.y, THEME_FG, THEME_BG, lang[MAPPING]);
    i = lenpalettes * (font->width + 1) + 2;
    strmaxw = screen->w - 5 - font->width;
    rect.x = 200; rect.w = i < screen->w - 205 - font->width ? i : screen->w - 205 - font->width; rect.h = font->height + 2;
    l = rect.w;
    SDL_FillRect(screen, &rect, theme[THEME_INPBG]);
    if(savepal < 0 || savepal >= numpalettes) savepal = 0;
    sdlprint(201,rect.y + 1, savefld == 1 ? THEME_INPUT : THEME_INACT, THEME_INPBG, palettes[savepal]);
    strmaxw = screen->w - 5;
    sdlprint(200 + rect.w, rect.y+1, THEME_BG, savefld == 1 ? THEME_INPUT : THEME_FG, "\031");

    rect.y = 20 + 3*font->height;
    sdlprint(42, rect.y, THEME_FG, THEME_BG, lang[MAPGENTYPE]);
    rect.x = 200; rect.w = font->width + font->width/2 + 2; rect.h = font->height + 2;
    SDL_FillRect(screen, &rect, theme[THEME_INPBG]);
    sdlprint(rect.x + font->width/4 + 1, rect.y + 1, savefld == 3 ? THEME_INPUT : THEME_FG, THEME_INPBG,
        savemapgen ? "x" : " ");

    rect.y = 24 + 4*font->height;
    sdlprint(42, rect.y, THEME_FG, THEME_BG, lang[BIOMETYPE]);
    rect.x = 200; rect.w = font->width + font->width/2 + 2; rect.h = font->height + 2;
    SDL_FillRect(screen, &rect, theme[THEME_INPBG]);
    sdlprint(rect.x + font->width/4 + 1, rect.y + 1, savefld == 4 ? THEME_INPUT : THEME_FG, THEME_INPBG,
        savebiome ? "x" : " ");

    rect.y = 28 + 5*font->height; rect.x = 42; rect.w = screen->w - 47;
    if(savebiome) {
        j = screen->h - 2*font->height - 8;
        rect.h = j - rect.y;
        SDL_FillRect(screen, &rect, theme[THEME_INPBG]);
        for(k = 0, i = 1; (int)i < numblocks; i++) {
            if(blocks[i].numref && blocks[i].blocknames && blocks[i].blocknames[0] && (rect.y + font->height) < j) {
                if(k >= biomescr) {
                    if(biomeblk == k) blocks[i].dobiome ^= 1;
                    strmaxw = screen->w / 2 - 4;
                    sdlprint(44,rect.y, savefld == 5 && biomepos == k ? THEME_INPUT : THEME_INACT, THEME_INPBG, blocks[i].name);
                    strmaxw = screen->w - 5;
                    sdlprint(screen->w/2,rect.y, savefld == 5 && biomepos == k ? THEME_INPUT : THEME_INACT, THEME_INPBG,
                        blocks[i].blocknames[blocks[i].dobiome ? 1 : (savemapgen && blocks[i].blocknames[2] &&
                            blocks[i].blocknames[2][0] ? 2 : savepal + 3)]);
                    rect.y += font->height;
                }
                k++;
            }
        }
        biomeblk = -1;
        if(k > maxbiome) maxbiome = k;
        if(!k) sdlprint(44, rect.y, THEME_INACT, THEME_INPBG, lang[ERR_NOBIOM]);
        rect.y = j + 4;
    }
    rect.h = font->height + 2;
    SDL_FillRect(screen, &rect, theme[savefld == 6 ? THEME_SAVEACT : THEME_SAVEINACT]);
    sdlprint((screen->w - 47 - mbstrlen(lang[SAVEBTN]) * (font->width+1)) / 2 + 42, rect.y+1,
        savefld == 6 ? THEME_INPUT : THEME_FG,
        savefld == 6 ? THEME_SAVEACT : THEME_SAVEINACT, lang[SAVEBTN]);
    if(savefld == 2) {
        rect.w = l + 8; rect.x = 200 + font->width; rect.y = 16 + 3*font->height; rect.h = font->height;
        for(i = 0; (int)i < numpalettes; i++, rect.y += rect.h) {
            SDL_FillRect(screen, &rect, theme[savepal == (int)i ? THEME_INPUT : THEME_INPBG]);
            sdlprint(rect.x + 4, rect.y, savepal == (int)i ? THEME_BG : THEME_FG, savepal == (int)i ? THEME_INPUT : THEME_INPBG,
                palettes[i]);
        }
    }
}

/**
 * Save file window scrolling event handler
 */
void save_scroll(SDL_Event *event)
{
    if(!savebiome) return;
    biomescr -= event->wheel.y;
    if(biomescr < 0) biomescr = 0;
    if(biomescr + 1 >= maxbiome) biomescr = maxbiome - 1;
}

/**
 * Save file window mouse down event handler
 */
void save_mousedown(SDL_Event *event)
{
    int i = savebiome ? screen->h - 2*font->height - 4 : 24 + 5*font->height;
    if(event->button.x < 42) return;
    if(savefld == 2) {
        if(event->button.x >= (int)(200 + font->width) && event->button.x < (int)(200 + (lenpalettes+1) * (font->width + 1)) &&
            event->button.y >= (int)(16 + 3*font->height) && event->button.y < (int)(16 + (3+numpalettes)*font->height)) {
                savefld = 3;
                savepal = (event->button.y - 16 - 3*font->height) / font->height;
        } else
            savefld = 1;
        return;
    }
    if(event->button.y >= (int)(12 + font->height) && event->button.y < (int)(16 + 2*font->height) && savefld) {
        savefld = 0; savepos = savelen = strlen(mtsfile);
    }
    if(event->button.y >= (int)(16 + 2*font->height) && event->button.y < (int)(20 + 3*font->height)) {
        savefld = 2;
    }
    if(event->button.y >= (int)(20 + 3*font->height) && event->button.y < (int)(24 + 4*font->height)) {
        savefld = 3;
        savemapgen ^= 1;
    }
    if(event->button.y >= (int)(24 + 4*font->height) && event->button.y < (int)(28 + 5*font->height)) {
        savefld = 4;
        savebiome ^= 1;
        biomepos = 0;
    }
    if(event->button.y >= i && event->button.y < (int)(i + font->height)) sdldo(1);
    else if(savebiome && event->button.y >= (int)(28 + 5*font->height) && event->button.y < i) {
        biomepos = biomescr + (event->button.y - (28 + 5*font->height)) / font->height;
        if(savefld != 5) savefld = 5;
        else biomeblk = biomepos;
    }
}

/**
 * Save file window key event handler
 */
void save_key(SDL_Event *event)
{
    int i, j;

    switch(event->type) {
        case SDL_TEXTINPUT:
            j = strlen(event->text.text);
            if(!savefld && savelen + j < (int)(sizeof(mtsfile))) {
                for(i = savelen - 1; i >= savepos; i--) mtsfile[i + j] = mtsfile[i];
                memcpy(mtsfile + savepos, &event->text.text, j);
                savepos += j;
                savelen += j;
                mtsfile[savelen] = 0;
            }
        break;
        case SDL_KEYUP:
            switch (event->key.keysym.sym) {
                case SDLK_TAB:
                    if(savefld == 6) savefld = 0; else
                    if(savefld == 1) savefld = 3; else savefld++;
                    if(!savebiome && savefld == 5) savefld++;
                break;
                case SDLK_BACKSPACE:
                    if(!savefld && savepos) {
                        savepos--;
                        for(i = savepos; i < savelen + 1; i++) mtsfile[i] = mtsfile[i+1];
                        savelen--;
                    }
                break;
                case SDLK_DELETE:
                    if(!savefld && savepos < savelen) {
                        for(i = savepos; i < savelen + 1; i++) mtsfile[i] = mtsfile[i+1];
                        savelen--;
                    }
                break;
                case SDLK_UP:
                    if(!savefld) savepos = 0;
                    if(savefld == 1 || savefld == 2) {
                        if(!savepal) savepal = numpalettes - 1; else savepal--;
                    }
                    if(savefld == 5) {
                        if(!biomepos && maxbiome) biomepos = maxbiome - 1; else biomepos--;
                        if(biomepos < biomescr) biomescr = biomepos;
                        i = ((screen->h - 2*font->height - 8) - (24 + 5*font->height)) / font->height;
                        if(biomepos >= biomescr + i) biomescr = biomepos - i;
                    }
                break;
                case SDLK_DOWN:
                    if(!savefld) savepos = savelen;
                    if(savefld == 1 || savefld == 2) {
                        if(savepal + 1 >= numpalettes) savepal = 0; else savepal++;
                    }
                    if(savefld == 5) {
                        if(biomepos + 1 >= maxbiome) biomepos = 0; else biomepos++;
                        if(biomepos < biomescr) biomescr = biomepos;
                        i = ((screen->h - 2*font->height - 8) - (24 + 5*font->height)) / font->height;
                        if(biomepos >= biomescr + i) biomescr = biomepos - i;
                    }
                break;
                case SDLK_LEFT:
                    if(!savefld && savepos) savepos--;
                    if(savefld == 1 || savefld == 2) {
                        if(!savepal) savepal = numpalettes - 1; else savepal--;
                    }
                break;
                case SDLK_RIGHT:
                    if(!savefld && savepos < savelen) savepos++;
                    if(savefld == 1 || savefld == 2) {
                        if(savepal + 1 >= numpalettes) savepal = 0; else savepal++;
                    }
                break;
                case SDLK_RETURN: sdldo(1); break;
                default:
                    if(savefld == 1) savefld = 2; else if(savefld == 2) savefld = 1;
                    if(savefld == 3) { savemapgen ^= 1; }
                    if(savefld == 4) { savebiome ^= 1; biomepos = 0; }
                    if(savefld == 5) { biomeblk = biomepos; }
                break;
            }
        break;
    }
}
