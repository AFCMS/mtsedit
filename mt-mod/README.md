MTSEdit Minetest Mod
====================

This is a little mod that connects MTSEdit to Minetest. With the "/mtsedit" chat command, you can generate the
[blocks.csv](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/blocks.md), which is used to correlate block types
between different Minetest games and Minecraft.

You can also save MTS files from your world, but most importantly, you can load the MTS files that you've created with MTSEdit.

Installation
------------

Just copy this "mtsedit" directory to your Minetest mods directory, for example "~/.minetest/mods".

If you don't have the source, only the installed executable, you can run
```
./mtsedit -i ~/.minetest/mods
```

### Dependencies

None, not even "default". This mod works on its own, has absolutely zero dependency.

__IMPORTANT__: the Lua API can't access files outside of the world directory, so you have to copy your existing blocks.csv
there (on Linux "~/.minetest/worlds/(world name)"), run the chat command, then copy the result back to /usr/share/mtsedit.

Likewise, you have to place your schematic files under "~/.minetest/worlds/(world name)/schems/", other directories are not
reachable from Lua.

Usage
-----

Enable the mtsedit mod. It does not depend on any game mods, so that you can use it with all games and all mods. Start your world,
then go to the chat (probably <kbd>F10</kbd> if you haven't changed the shortcut).

Type
```
/mtsedit
```
or
```
/mtsedit help
```
to get help. When you first run this command, it will give you a "MTSEdit Magic Placement Wand Tool" (TM) :-)
<img src="https://gitlab.com/bztsrc/mtsedit/raw/master/mt-mod/mtsedit/textures/mtsedit_place.png">

### Importing MTS files

For that, enter chat and run

```
/mtsedit load (file name without .mts)
```
Exit chat (with <kbd>Esc</kbd>) and use the MTSEdit Wand with right clicking on a node where you want to build the structure.
That's all. Depending on which direction you're looking at your punched node, the structure's orientation will change. You can
repeat this right click as many times as you want. The same structure will be placed until you issue another "/mtsedit load"
command.

To help you with the names,

```
/mtsedit list
```
will list the available MTS files in your world directory's "~/.minetest/worlds/(my world)/schems/" sub-dir. These are the ones
that you can use with the "load" command.

### Exporting MTS files

First, select one corner of the area that you want to save: left click on a node with the MTSEdit Wand. Then go to the opposite
corner of the area (you probably want to get fly priv), and left click on the air (without selecting any particular node). A
green selection cube will appear arround the area. You can modify this as many times as you want. If by any chance you can't
reach the exact edges of the desired area, then you can grow (or with negative values, shrink) it using the following chat command:

```
/mtsedit grow (delta)
```

When you're satisfied with the selection, go to the chat window, and

```
/mtsedit save (file name without .mts)
```
will save that area into an MTS file into the "schems" directory. After that you can open it with MTSEdit outside of the game.

### Exporting node data for MTSEdit

In the chat window issue the following command

```
/mtsedit export Minetest Game
```
Where the argument is one of the coloumns in block.csv, so called node palette (or block mapping). This name is totally up to you,
and you'll be able to select it when you save MTS files in the editor. I suggest to use a name that reminds you of the game + mods
combination, like "MTG with moretrees". If the blocks.csv exists in the world directory, then this mod will read it, and it will
only update that particular one coloumn in it. If the csv file does not exists, then it will be created. If the csv exists, but
there's no such mapping as specified in the argument, then a new coloumn will be created by that name.

The Lua script tries to figure out which node is which, but because you can enable lots of mods in Minetest (each allowed to
register it's own node types), that's not an easy task. It does a pretty decent job though, usually about 80% of the records
are correct, and only 20% percent requires manual fixing. For Mineclone2, I've provided an almost entirely complete blocks.csv,
which also contains the Minecraft NBT .schematic files' Block IDs.

To export node texture data, use
```
/mtsedit imgs
```
This will save blockimgs.csv in the world directory, that you can import with `-t` to generate the block images for the editor.
This is useful if you have enabled mods that register new nodes which have no editor images yet.

Enjoy!
