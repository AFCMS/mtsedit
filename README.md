Minetest MTS Editor
===================

<img src="https://gitlab.com/bztsrc/mtsedit/raw/master/etc/mtsedit.png"> This is a simple editor for
[Minetest](https://www.minetest.net) Schematic files ([MTS](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/mts_format.md))

Batch Mode
----------

You can convert MTS files and [other formats](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/import.md), generate preview
images or blueprints for them from a script with this tool. You can also generate the block images for the editor.
[Read more](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/batch.md).

Interactive Mode
----------------

Actually the editor consist of two independent components. One is running outside of the game, and since it's a GUI application,
I think a picture can tell more than thousand words. Read the editor's [User Manual](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/usage.md)
for more details.

<img src="https://gitlab.com/bztsrc/mtsedit/raw/master/docs/mtsedit.png">

The User Interface is translatable. Supported languages: English, Magyar, Español, Français, Deutch, Polski, Русский, Português
and Italiano.

The other component is a Minetest mod, under the [mt-mod](https://gitlab.com/bztsrc/mtsedit/tree/master/mt-mod) directory. This
small Lua script runs inside the game, and can import your edited MTS files into the game's world. It can also save MTS files
and it's capable of generating the blocks data required by the editor (if the provided definitons are not enough for you).

<img src="https://gitlab.com/bztsrc/mtsedit/raw/master/docs/mtseditmod.png">

Installation
------------

To install from source, read [compilation instructions](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/compile.md).

### Under Linux

1. download [mtsedit-x86_64-linux.tgz](https://gitlab.com/bztsrc/mtsedit/raw/master/mtsedit-x86_64-linux.tgz)
2. as root, extract it to "/" (if you don't trust it, check the tarball first): "sudo tar -C / -xzvf mtsedit-x86_64-linux.tgz"
3. I guess you know your distro enough to assign a program to an extension. There are many ways, but if your desktop environment
recognizes the MTSEdit application from the installed .desktop file, then file assosiation has already taken care for you.
4. if the MTSEdit application appears in the application menu, but .mts files are still not recognized, then as root add the
```
model/vnd.mts                mts schematic
```
line to "/etc/mime.types".

### Under Windows

1. download [mtsedit-i686-win.zip](https://gitlab.com/bztsrc/mtsedit/raw/master/mtsedit-i686-win.zip)
2. unpack it into "C:\\Program Files (x86)"
3. copy "C:\\Program Files (x86)\\MTSEdit\\MTSEdit.lnk" to your desktop or into your menu directory (whereever you like)
4. open My Computer, then from the "Tools" menu, select "Folder Options"
5. click on "File Types" tab
6. scroll down to "MTS" (if you can't find it, click on "New" button and add it)
7. click on "Change" button
8. click on "Browse..." button
9. choose "C:\\Program Files (x86)\\MTSEdit\\mtsedit.exe"
10. check "Always use the selected program to open this kind of file"
11. click on "Ok" button

### Under MacOSX

1. download [SDL2-X.dmg](http://libsdl.org/download-2.0.php) under Runtime Binaries
2. open it with Finder, and drag'n'drop SDL2.framework to "/Library/Frameworks"
3. download [mtsedit-intel-macosx.zip](https://gitlab.com/bztsrc/mtsedit/raw/master/mtsedit-intel-macosx.zip)
4. unpack it into "/Applications"
5. in the Finder, find an .mts file
6. pull down the "File" menu and select "Get info" or press <kbd>Command</kbd>+<kbd>I</kbd>
7. expand the "Open With" submenu
8. choose "MTSEdit.app"
9. click on "Change All..." button

### Installing the Minetest mod (common to all OS)

To install the Minetest mod, run the following command in a Terminal using your Minetest mods directory as parameter, for example:
```
mtsedit -i ~/.minetest/mods
```
or
```
mtsedit.exe -i "C:\\Program Files (x86)\Minetest\mods"
```

Enjoy!

Known Bugs
----------

Zooming can be slow, because it scales blocks in run-time. Using a cache would require double the memory footprint. I suggest
to edit on 100% zoom, as that's using a different, fast blitting function.

Rotations are picked numerically, not necessarily in rotation CW / CCW order (but you can set all rotations, and they will be
saved correctly, it's just you might have to click more to get the one you want).

bzt
