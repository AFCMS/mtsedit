MTSEdit Block Types Data
========================

Directory Paths
---------------

Block definitions are looked for in several directories, even depending on operating system. Regardless if you set the
`MTSDATA` environment variable, you can specify the directory explicitly.

1. the directory specified in '$MTSDATA' if enviroment variable defined (Linux, MacOSX, Windows)
2. in the user's home directory '$HOME/.config/mtsedit' (Linux)
3. in the user's home directory '$HOME/Library/Application Support/MTSEdit' (MacOSX)
4. system wide directory '/usr/share/mtsedit' or '/usr/local/share/mtsedit' (Linux and MacOSX)
5. system wide directory '/Applications/MTSEdit.app/Contents/Resources/data' (MacOSX only)
5. in the executable's directory, most likely 'C:\\Program Files (x86)\\MTSEdit\\data' (Windows only)
6. in the executable's parent directory, '../data' (source repository, and if installed to '/opt/mtsedit/bin' on Linux)

Blocks.csv
----------

In the `data` directory, mtsedit looks for this CSV file. It defines block types used in different file formats.
Each line contains comma separated values, which might be surrounded by quotes. Commas inside quoted strings does
not count as coloumn separators. The lines are ended in '\\n' newline character, carrige return '\\r' is optional.
This database can be generated on the fly with [MTSEdit Minetest Mod](https://gitlab.com/bztsrc/mtsedit/tree/master/mt-mod)
from within the game (with all your desired mods enabled).

### Header

The first row is special, it defines the coloumn names. The first 5 coloumns are irrelevant and not parsed, others
define the name of the block mappings (each coloumn is one mapping). These are the names used by the "/mtsedit export"
chat command, and they are also shown on the Save As window.

### Coloumns

| Number | Format  | Description                                                                           |
|-------:|---------|---------------------------------------------------------------------------------------|
|    1st | string  | block id, canonical name of the block (also used to identify block images)            |
|    2nd | intlist | list of slash '/' separated integer numbers, the Minecraft BlockID in schematic files |
|    3rd | string  | list of slash '/' spearated strings, the Sponge BlockName in schematic files          |
|    4th | string  | an optional biome specific name for the block (like "biome:node_stone")               |
|    5th | string  | an optional mapgen alias name for the block (like "mapgen_stone")                     |
|   6th+ | string  | block's name in the mapping (this coloumn may be repated as many times as you like)   |

Block mappings are sometimes referenced as node palette. The repository ships a default blocks.csv with two coloumns,
mappings for "Mineclone2" and "Minetest Game", but feel free to add as many more mappings as you wish.

For file formats that rely on numeric IDs, the values in the 2nd coloumn are used. These can be however overriden on a
per schematic file basis using the `-r` flag.

An example line (with Mineclone2 in 6th and MineTest Game in 7th coloumn) would look like this:
```
Stone,1,minecraft:stone,biome:node_stone,mapgen_stone,mcl_core:stone,default:stone
```
The 2nd and 3rd coloumns may have more values, like
```
Leaves,18,minecraft:leaves/minecraft:oak_leaves,,,mcl_core:leaves,default:leaves
```

Block Images
------------

Each block type should have one image in the `data` directory. These are named as:
```
(block id).png
```
The "block id" cames from the canonical name (1st coloumn), but with spaces converted to underscores '\_'. This image must be
32 pixels wide, and its height must be multiple of 32. How many 32 x 32 icons it contains specifies how many `param2` values
are shown (regardless you can set all param2 values). When there are more block icons, then their positions are as follows:
y = 0: the block is facing North, y = 32: rotated by 90 degrees clockwise (facing East), y = 64: rotated by 180 degrees (faces
South), y = 96: rotated by 90 degrees counter-clockwise (faces West). If they exists, icons for param values 04-1F are shown
too, otherwise the existing icons are repeated.

Hint: the MTSEdit tool can generate slab and stair images from block pictures using the `-g` option. This will only generate the
first 4 orientation, a slab, and a mirrored verion of the original block. The `-t` uses the textures from the game, and it can
generate much more variants.

Translations
------------

The MTSEdit user interface is multilingual, but blocks.csv is not. If you want to translate block names too, you'll have to
provide translation files in the `data` directory. These are plain text files, by the name "blocks_(lang).txt", and with
lines each of "(block id)=(translated name)". As with blocks.csv, the lines are ended by a newline '\\n' character, '\\r'
is optional.

Texture Data CSV
----------------

Saved in blockimgs.csv by the [MTSEdit Minetest Mod](https://gitlab.com/bztsrc/mtsedit/tree/master/mt-mod), and read by the
`-t` option. The coloumns are as follows:
- block id (canonical name, same as block.csv's 1st coloumn)
- block's technical name (same as one of the mapping coloumns)
- drawtype (normal,plantlike,raillike etc. where to put the textures on the cube)
- paramtype2 (none,regular,facedir,wallmounted etc. rotation options)
- node_box type (if any)
- directory of the textures
- list of texture file names
- an "|" pipe separator
- list of overlay texture file names
