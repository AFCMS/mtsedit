Minetest MTS Editor Compilation
===============================

This is very simple, as MTSEdit was written in ANSI C. The one and only dependency it has is the SDL2 library. Just issue `make`
in the "src" directory, and that will autodetect your operating system (Linux, BSD, MacOSX and Windows).

It also needs some [data files](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/blocks.md), which can be found
[here](https://gitlab.com/bztsrc/mtsedit/tree/master/data).

### Under Linux, BSD and all other POSIX systems

You'll need the development version of SDL2, which can be installed by your distro's standard package management software
("apt-get install", "emerge", "pacman", "port", "homebrew" etc.). It is usually called "libsdl-dev" or similar.

1. in the mtsedit/src/ directory, run `make`
2. run `sudo make install` (this will copy mtsedit into /usr/bin, and the data/ directory into /usr/share/mtsedit)

### Under MacOSX

1. in a Terminal, run `xcode-select --install` and in the pop-up window click on "Install", this will give you "gcc" and "make" under MacOSX
2. download [SDL2-X.dmg](http://libsdl.org/download-2.0.php) under the section Development Libraries
3. open it with Finder, and drag'n'drop SDL2.framework to "/Library/Frameworks"
4. in the mtsedit/src/ directory, run `make`
5. run `make package`
6. extract ../mtsedit-intel-macosx.zip to "/Applications"

### Under Windows

1. install [MinGW](https://osdn.net/projects/mingw/releases), this will give you "gcc" and "make" under Windows
2. download [SDL2-devel-X-mingw.tar.gz](http://libsdl.org/download-2.0.php) under the section Development Libraries
3. extract SDL2 into a directory under MinGW's home directory
4. open mtsedit/src/Makefile in Notepad, and edit MINGWSDL to the path where you've extracted the tarball, add the last SDL2-X part too
5. copy $(MINGWSDL)/i686-w64-mingw32/bin/SDL2.dll into C:\\Windows
6. start MSYS (part of MinGW), go to the mtsedit/src/ directory, and run `make` (if it complains about "cc", then run `CC=gcc make`)
7. create "C:\\Program Files (x86)\\MTSEdit"
8. copy the newly compiled mtsedit.exe there
9. copy the  mtsedit/data/ directory to "C:\\Program Files (x86)\\MTSEdit\\data"

